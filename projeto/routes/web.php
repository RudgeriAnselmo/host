<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Inicio
Route::get('/', function () {
    return view('index');
});
Route::get('/cadastro', function () {
    return view('cadastro');
});

Route::get('/logado/{enviar}', function ($enviar) {
    return view('logado'.$enviar);
});

//Usuario
Route::post('up', 'UsuarioController@cadastrar');
Route::post('login','UsuarioController@login');
Route::post('logout','UsuarioController@logout');
//Postagens
Route::post('postar','PostagemController@postar');
Route::post('comentar','PostagemController@comentar');
Route::get('listar','PostagemController@listar');
Route::get('listarComentario','PostagemController@listarComentario');
Route::post('comentar','PostagemController@comentar');


