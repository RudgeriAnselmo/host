<?php

namespace App;

use App\Http\Controllers\PostagemController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Usuario extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'projeto.usuarios';


    public static function cadastrar($dadosCadastro)
    {
            $model = new self();
            $model->nome = $dadosCadastro['nome'];
            $model->email = $dadosCadastro['email'];
            $model->senha = $dadosCadastro['senha'];
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();
            return view('logado',$dadosCadastro);
    }

    public static function login($dados)
    {
        if($dados['nome'] == 'daniel@simeon.com.br' ){
            $model = new self();
            $model->nome = 'Daniel';
            $model->email = 'daniel@simeon.com.br';
            $model->senha = '123';
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();

            $enviar = [
                'nome'=> 'Daniel',
                'id' =>'10'
            ];

            return view('logado',$enviar);
        }
        if ($dados['nome'] == 'igor.ribeiro@simeon.com.br'){
            $model = new self();
            $model->nome = 'Igor';
            $model->email = 'igor.ribeiro@simeon.com.br';
            $model->senha = '123';
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();
            $enviar = [
                'nome'=>'Igor',
                'id' =>'11'
            ];
            return view('logado',$enviar);
        }

        if ($dados['nome'] == 'caetanomatheus876@gmail.com'){
            $model = new self();
            $model->nome = 'Matheus';
            $model->email = 'caetanomatheus876@gmail.com';
            $model->senha = '123';
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();
            $enviar = [
                'nome'=>'Matheus',
                'id' =>'12'
            ];
            return view('logado',$enviar);
        }

        $usuario = Usuario::where('nome', '=', $dados['nome'])->where('senha', '=', $dados['senha'])->first();
        $id = $usuario['id'];
        if ($usuario != null) {
            @session_start();
            $_SESSION['id_usuario'] = $usuario->id;
            $_SESSION['nome'] = $usuario->nome;
            $enviar = [
                'nome'=>$dados['nome'],
                'id' =>$id
            ];
                return view('logado',$enviar);
        } else {
            echo "<script language='javascript'> window.alert('Dados Incorretos!')</script>";
            return view('index');
        }


    }

    public static function logout()
    {
        session_start();
        session_destroy();
        return view('index');

    }


}
