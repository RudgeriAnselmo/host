<?php

namespace App;

use App\Http\Controllers\UsuarioController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Fluent;
use Carbon\Carbon;
use function Symfony\Component\String\u;

class Postagem extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'projeto.postagens';

    public static function postar($dadosPost)
    {
        $usuario = Usuario::where('nome', '=', $dadosPost['nome'])->first();
        $model = new self();
        $model->id_usuario = $usuario['id'];
        $model->nome_autor = $usuario['nome'];
        $model->titulo = $dadosPost['titulo'];
        $model->descricao = $dadosPost['descricao'];
        $model->created_at = date('Y-m-d H:i:s');
        $model->save();
        $model['nome'] = $usuario['nome'];
        return view('logado', $model);


    }

    public static function listar()
    {

        $query = Postagem::query()->paginate();
        $dados = $query->toArray();
        $postagens = $dados['data'];

        foreach ($postagens as $d) {
            $filtro = $dados['data'];
            return $filtro;
        }

        return;
    }

    public static function comentar(ComentarPost $request)

    {

        $betaUser = \Auth::user();

        $comentario = new ComentarPost();

        $comentario->comentario = $request->comentario;
        $comentario->comentado_em = Carbon::now();
        $comentario->comentado_por = $betaUser->id;
        $comentario->post_id = $request->post_id;

        if ((int)$request->comentario_pai_id) {
            $comentario->comentario_pai_id = $request->comentario_pai_id;
        }

        $comentario->save();

        return $comentario;

    }

    public static function listarComentarios(ListarComentario $request)
    {

        if ((int)$request->post_id) {

            return PostsComentarios::wherePostId($request->post_id)
                ->whereNull('comentario_pai_id')
                ->orderBy('comentado_em', 'DESC')
                ->with('reComentarios.usuario')
                ->with('usuario')
                ->get();

        }

    }


}

