<?php

namespace App\Http\Controllers;

use App\Postagem;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Usuario;

class UsuarioController extends Controller
{
    public function cadastrar(Request $request)
    {

        $dadosCadastro = $request->all();
        \DB::beginTransaction();
        $cadastro = Usuario::cadastrar($dadosCadastro);
        \DB::commit();
        return $cadastro;
    }

    public function login(Request $request)
    {
        $dados = $request->all();
        \DB::beginTransaction();
        $logado = Usuario::login($dados);
        \DB::commit();
        return $logado;
    }

    public function logout()
    {
        $logout = Usuario::logout();
        return $logout;

    }


}
