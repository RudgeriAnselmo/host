<?php

namespace App\Http\Controllers;
use App\ComentarPost;
use Illuminate\Routing\Controller;
use App\Postagem;
use App\Usuario;
use Illuminate\Http\Request;

class PostagemController extends Controller
{
    public function postar(Request $request)
    {
        $dadosPost = $request->all();
        \DB::beginTransaction();
        $postado = Postagem::postar($dadosPost);
        \DB::commit();
        return $postado;
    }

    public function listar(Request $request)
    {
        \DB::beginTransaction();
        $listar = Postagem::listar();
        \DB::commit();
        return $listar;

    }
    public function comentar(ComentarPost $request)
    {
        \DB::beginTransaction();
        $comentado = Postagem::comentar($request);
        \DB::commit();
        return $comentado;
    }
    public function listarComentarios(Request $request)
    {
        \DB::beginTransaction();
        $list = Postagem::listarComentarios($request);
        \DB::commit();
        return $list;
    }

}

