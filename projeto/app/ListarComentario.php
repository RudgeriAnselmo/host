<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class ListarComentario extends Model
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuario_id' => 'int',
            'comentario_id' => 'int',
            'comentario_pai_id' => 'int',
            'post_id' => 'int|registro_existe:projeto.comentarios,id'
        ];
    }


}
