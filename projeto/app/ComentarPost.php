<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;
Use App\Postagem;

class ComentarPost extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'projeto.comentarios';

    public $timestamps = false;

    public $appends = [];

    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'id', 'comentado_por');
    }

    public function post()
    {
        return $this->hasOne(Postagem::class, 'id', 'post_id');
    }
    public function reComentarios()
    {

        return $this->hasMany(ComentarPost::class, 'comentario_pai_id', 'id');

    }
}
